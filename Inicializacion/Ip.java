package Inicializacion;
public class Ip {
    private String ip, tipo, codigo;
    
    //constructor
    public Ip(String ip, String tipo, String codigo){
        this.ip = ip;
        this.tipo = tipo;
        this.codigo = codigo;
    }
    
    //setters
    public void setIp(String ip){
        this.ip = ip;
    }   
    public void setTipo(String tipo){
        this.tipo = tipo;
    }    
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }    
    
    //getters
    public String getIp(){
        return ip;
    }
    public String getTipo(){
        return tipo;
    }
    public String getCodigo(){
        return codigo;
    }
}