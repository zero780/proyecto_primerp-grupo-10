package Inicializacion;
public class Proveedor extends Persona{  
  
    //constructor 1 (para inicializar ArrayList de información)
    public Proveedor(String codigo, String nombre, String direccion, String telefono){
        super(codigo,nombre,direccion,telefono);
    }
    
    //constructor 2 (para generar la factura final)
    public Proveedor(String mes, String año, String codigo, String codPais,int llamComp, int llamInte, float totalPago) {
        super(mes, año, codigo, codPais, llamComp, llamInte, totalPago);
    }       
}