package Inicializacion;
public class Pais {
    private String codigo, nombre, prefijo;
    
    //constructor
    public Pais(String codigo, String nombre, String prefijo){
        this.codigo = codigo;
        this.nombre = nombre;
        this.prefijo = prefijo;
    }
    
    //setters
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }   
    public void setNombre(String nombre){
        this.nombre = nombre;
    }    
    public void setPrefijo(String prefijo){
        this.prefijo = prefijo;
    }      
    
    //getters
    public String getCodigo(){
        return codigo;
    }
    public String getNombre(){
        return nombre;
    }
    public String getPrefijo(){
        return prefijo;
    }
}
