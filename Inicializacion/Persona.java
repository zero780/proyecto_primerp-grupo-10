package Inicializacion;
public abstract class Persona {
   private String codigo, nombre, direccion, telefono, mes, año, codPais; 
   private int llamComp, llamInte; private float totalPago;

   //constructor 1 (para inicialziar los ArrayList de información)
   public Persona(String codigo, String nombre, String direccion, String telefono){
      this.codigo = codigo;
      this.nombre = nombre;
      this.direccion = direccion;
      this.telefono = telefono;
   }
   
   //constructor 2 (para generar la factura final)
   public Persona(String mes, String año, String codigo, String codPais,int llamComp, int llamInte, float totalPago) {
      this.mes = mes;
      this.año = año;
      this.codigo = codigo;
      this.codPais = codPais;
      this.llamComp = llamComp;
      this.llamInte = llamInte;
      this.totalPago = totalPago;
    }
    
    //setters 1
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }   
    public void setNombre(String nombre){
        this.nombre = nombre;
    }    
    public void setDireccion(String direccion){
        this.direccion = direccion;
    }    
    public void setTelefono(String telefono){
        this.telefono = telefono;
    } 
    
    //getters 1
    public String getCodigo(){
        return codigo;
    }
    public String getNombre(){
        return nombre;
    }
    public String getDireccion(){
        return direccion;
    }
    public String getTelefono(){
        return telefono;
    }
    
    //setters 2 
    public String getMes() {
        return mes;
    }
    public String getAño() {
        return año;
    }

    public String getCodPais() {
        return codPais;
    }
    public int getLlamComp() {
        return llamComp;
    }
    public int getLlamInte() {
        return llamInte;
    }
    public float getTotalPago() {
        return totalPago;
    }
         
    //getters 2
    public void setMes(String mes) {
        this.mes = mes;
    }
    public void setAño(String año) {
        this.año = año;
    }
    public void setCodPais(String codPais) {
        this.codPais = codPais;
    }
    public void setLlamComp(int llamComp) {
        this.llamComp = llamComp;
    }
    public void setLlamInte(int llamInte) {
        this.llamInte = llamInte;
    }
    public void setTotalPago(float totalPago) {
        this.totalPago = totalPago;
    }
}