package Inicializacion;
public class Llamada {
    private String fecha, hora, ip_fuente, ip_destino, dnis, ani, estado, duración;
    
    //constructor
    public Llamada(String fecha, String hora, String ip_fuente, String ip_destino, String dnis,String ani,String estado,String duración){
        this.fecha = fecha;
        this.hora = hora;
        this.ip_fuente = ip_fuente;
        this.ip_destino = ip_destino;
        this.dnis = dnis;
        this.ani = ani;
        this.estado = estado;
        this.duración = duración;
    }
    
    //setters
    public void setFecha(String fecha){
        this.fecha = fecha;
    }   
    public void setHora(String hora){
        this.hora = hora;
    }    
    public void setIpF(String ip_fuente){
        this.ip_fuente = ip_fuente;
    }    
    public void setIpD(String ip_destino){
        this.ip_destino = ip_destino;
    } 
    public void setDNIS(String dnis){
        this.dnis = dnis;
    }
    public void setANI(String ani){
        this.ani = ani;
    }
    public void setEstado(String estado){
        this.estado = estado;
    }
    public void setDuracion(String duración){
        this.duración = duración;
    }
    
    //getters
    public String getFecha(){
        return fecha;
    }   
    public String getHora(){
        return hora;
    }    
    public String getIpF(){
        return ip_fuente;
    }    
    public String getIpD(){
        return ip_destino;
    } 
    public String getDNIS(){
        return dnis;
    }
    public String getANI(){
        return ani;
    }
    public String getEstado(){
        return estado;
    }
    public String getDuracion(){
        return duración;
    }
}


