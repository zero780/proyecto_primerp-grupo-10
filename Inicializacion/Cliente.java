package Inicializacion;
public class Cliente extends Persona{
    private String tipo;
    
    //constructor 1 (para inicializar ArrayList de información)
    public Cliente(String codigo, String nombre, String direccion, String telefono, String tipo){
        super(codigo,nombre,direccion,telefono);
        this.tipo = tipo;
    }
    
    //constructor 2 (para generar la factura final)
    public Cliente(String mes, String año, String codigo, String codPais,int llamComp, int llamInte, float totalPago) {
        super(mes, año, codigo, codPais, llamComp, llamInte, totalPago);
    }      
       
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }   
}
