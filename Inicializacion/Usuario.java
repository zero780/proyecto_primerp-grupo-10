package Inicializacion;
public class Usuario {
    private String usuario, contraseña, nombre, nivel;
    
    //constructor
    public Usuario(String usuario, String contraseña, String nombre, String nivel){
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.nombre = nombre;
        this.nivel = nivel;
    }
    
    //setters
    public void setUsuario(String usuario){
        this.usuario = usuario;
    }   
    public void setContraseña(String contraseña){
        this.contraseña = contraseña;
    }    
    public void setNombre(String nombre){
        this.nombre = nombre;
    }    
    public void setNivel(String nivel){
        this.nivel = nivel;
    } 
    
    //getters
    public String getUsuario(){
        return usuario;
    }
    public String getContraseña(){
        return contraseña;
    }
    public String getNombre(){
        return nombre;
    }
    public String getNivel(){
        return nivel;
    }   
}
