package Inicializacion;
public class Tarifa {
    private String codigoPro, prefijoPais, nombreRegion, prefijoRegion, tarifa, codTarifa;
    
    //constructor
    public Tarifa(String codigoPro, String prefijoPais, String nombreRegion, String prefijoRegion, String tarifa, String codTarifa){
        this.codigoPro = codigoPro;
        this.prefijoPais = prefijoPais;
        this.nombreRegion = nombreRegion;
        this.prefijoRegion = prefijoRegion;
        this.tarifa = tarifa;
        this.codTarifa = codTarifa;
    }
    
    //setters
    public void setcodigoPro(String codigoPro){
        this.codigoPro = codigoPro;
    }   
    public void setprefijoPais(String codPais){
        this.prefijoPais = codPais;
    }
    public void setnombreRegion(String nombreRegion){
        this.nombreRegion = nombreRegion;
    }    
    public void setPrefijoRegion(String prefijoRegion){
        this.prefijoRegion = prefijoRegion;
    } 
    public void setTarifa(String tarifa){
        this.tarifa = tarifa;
    }
    public void setCodTarifa(String codTarifa){
        this.codTarifa = codTarifa;
    }
    
    //getters
    public String getcodigoPro(){
        return codigoPro;
    }   
    public String getprefijoPais(){
        return prefijoPais;
    }    
    public String getnombreRegion(){
        return nombreRegion;
    }    
    public String getPrefijoRegion(){
        return prefijoRegion;
    } 
    public String getTarifa(){
        return tarifa;
    }
    public String getCodTarifa(){
        return codTarifa;
    }
}
