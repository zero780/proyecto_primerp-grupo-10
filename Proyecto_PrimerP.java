import Inicializacion.*;
import Metodos.*;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import java.util.Scanner;
public class Proyecto_PrimerP {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        //ARRAYS INICIALES DE INFORMACIÓN
        ArrayList<Usuario> userInfo = Lectura.leerUsuario();
        ArrayList<Cliente> clienteInfo = Lectura.leerCliente();
        ArrayList<Proveedor> proveedorInfo = Lectura.leerProveedor();
        ArrayList<Pais> paisInfo = Lectura.leerPais();
        ArrayList<Ip> ipInfo = Lectura.leerIp();
        ArrayList<Tarifa> tarifaInfo = Lectura.leerTarifa();
               
        //INICIAR SESION
        String usuario, contraseña; int cont = 0; 
        while (cont!=1){
            usuario = JOptionPane.showInputDialog(null, 
                "Ingrese su nombre de Usuario: ", "INICIO DE SESIÓN",3);
            contraseña = JOptionPane.showInputDialog(null, 
                    "Ingrese su contraseña: ", "INICIO DE SESIÓN",3);
            String opcion = ""; String menustr = "";
            Metodo method = new Metodo();
            boolean verificacion1 = method.verificarLogin(userInfo, usuario,contraseña);
            if (verificacion1 ==true){
                JOptionPane.showMessageDialog(null, 
                    "Inicio de Sesión Satisfactorio", "MENSAJE",JOptionPane.INFORMATION_MESSAGE);            
                String nivel = method.obtenerUser(userInfo, usuario).getNivel();
                if (nivel.equals("tecnico")){                   
                    //MENU DE TECNICO
                    menustr = method.obtenerMenuT();
                    while (!opcion.equals("7")){
                        System.out.println("-----------------------------------"
                                +"\nBIENVENIDO AL MENÚ TÉCNICO\n"+menustr);
                        System.out.println("Ingrese una opción: ");
                        opcion = entrada.next();
                        System.out.println("-----------------------------------");
                        switch (opcion) {
                            //1. INFORMACION DE CLIENTES
                            case "1":
                                System.out.println("\n1. INFORMACIÓN DE CLIENTES\n");                                                               
                                method.elegirdeClientes(clienteInfo);
                                break;

                            case "2":
                                System.out.println("\n2. -​ ​INFORMACION DE PROVEEDORES\n");                                                               
                                method.elegirdeProveedores(proveedorInfo);
                                break;

                            case "3":
                                System.out.println("\n3. - INFORMACIÓN DE PAÍSES\n");                                                               
                                method.elegirdePaises(paisInfo);
                                break;

                            case "4": 
                                System.out.println("\n4. - ADMINISTRAR IPS\n");                                                               
                                method.elegirdeIps(ipInfo, clienteInfo, proveedorInfo);
                                break;

                            case "5":
                                System.out.println("\n5. INFORMACIÓN DE TARIFA DE REGIÓN\n");                                                               
                                method.elegirdeTarifas(tarifaInfo, proveedorInfo);
                                break;

                            case "6":                        
                                System.out.println("\n6. FACTURAR LLAMADAS\n");
                                method.facturarLlamadas(ipInfo,tarifaInfo,clienteInfo);
                                File f = new File("archivos/llamadas_facturadas.csv");
                                JOptionPane.showMessageDialog(null, 
                                "El Proceso de creación ha finalizado\n"
                                + "La ruta de su archivo es: "+f.getAbsolutePath(), 
                                "PROCESO COMPLETO",JOptionPane.INFORMATION_MESSAGE);
                                break;

                            case "7":
                                opcion = "7";                                
                                break;
                                
                            default:
                                System.out.println("Selección Incorrecta");
                                break;
                        }
                    }                                                                                               
                }
                
                else if (nivel.equals("admin")){
                    //MENU DE ADMIN                   
                    File f = new File("archivos/llamadas_facturadas.csv"); 
                    menustr = method.obtenerMenuA(); int cont2 = 0;
                    if (!f.exists()){
                        System.out.println("\nNo hay información de facturación\n"); 
                        cont2++;
                    }                 
                    while (cont2==0){
                        while (!opcion.equals("4")) {
                            System.out.println("-----------------------------------"
                                +"\nBIENVENIDO AL MENÚ ADMIN\n"+menustr);
                            //sumarios
                            ArrayList<Cliente> listClienteF = Lectura.crearSumarioC();
                            ArrayList<Proveedor> listProveeF = Lectura.crearSumarioP();
                            ArrayList<String> listMeses = Lectura.obtenerMeses();
                            
                            System.out.println("Ingrese una opción: ");
                            opcion = entrada.next();                           

                            switch (opcion) {
                                //1. INFORMACION DE CLIENTES
                                case "1":                                   
                                    System.out.println("\n1. DETALLE FACTURA DE CLIENTE\n");                                                               
                                    method.mostrarFactura(clienteInfo, listClienteF, listMeses);
                                    break;

                                case "2":
                                    System.out.println("\n2. ​ REPORTE LLAMADAS ​POR CUENTAS POR MES\n");                                                               
                                    method.mostrarReporteMesC(clienteInfo, listClienteF, listMeses,paisInfo);                
                                    break;

                                case "3":
                                    System.out.println("\n3. ​REPORTE LLAMADAS POR PROVEEDOR POR MES\n");
                                    method.mostrarReporteMesP(proveedorInfo, listProveeF, listMeses, paisInfo);
                                    break;

                                case "4": 
                                    opcion = "4";
                                    cont=0;
                                    cont2++;
                                    break;
                                    
                                default:
                                    System.out.println("Selección Incorrecta");
                                    break;
                            }                       
                        }
                    }                                     
                }
                else {
                    JOptionPane.showMessageDialog(null, 
                        "Usuario o contraseña no Válida ", "ERROR EN INICIO DE SESIÓN",JOptionPane.ERROR_MESSAGE);
                }
            }
        }      
    }
}