package Metodos;
import Inicializacion.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
public class Lecturas {
    //LEER ARCHIVO
    //PARA USUARIOS
    public static ArrayList<Usuario> leerUsuario(){
        ArrayList<Usuario> userInfo = new ArrayList<>();
        String csvFile = "usuarios.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] datos = line.split(cvsSplitBy);
                Usuario user = new Usuario(datos[0], datos[1], datos[2], datos[3]);
                userInfo.add(user);
            }           
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }                       
        return userInfo;
    }    
    
    //PARA CLIENTES
    public static ArrayList<Cliente> leerCliente(){
        ArrayList<Cliente> clienteInfo = new ArrayList<>();
        String csvFile = "clientes.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
            String[] datos = line.split(cvsSplitBy);
            Cliente cliente1 = new Cliente(datos[0], datos[1], datos[2], datos[3], "Wholesale o Retail");
                clienteInfo.add(cliente1);
            }           
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }                       
        return clienteInfo;
    }
    
    //PARA PAISES
    public static ArrayList<Pais> leerPais(){
        ArrayList<Pais> paisInfo = new ArrayList<>();
        String csvFile = "paises.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
            String[] datos = line.split(cvsSplitBy);
            Pais pais1 = new Pais(datos[0], datos[1], datos[2]);
                paisInfo.add(pais1);
            }           
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }                       
        return paisInfo;
    }
    
    //PARA IPs
    public static ArrayList<Ip> leerIp(){
        ArrayList<Ip> ipInfo = new ArrayList<>();
        String csvFile = "ips.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
            String[] datos = line.split(cvsSplitBy);
            Ip ip1 = new Ip(datos[0], datos[1], datos[2]);
                ipInfo.add(ip1);
            }           
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }                       
        return ipInfo;
    }
    
    //PARA TARIFAS 
    public static ArrayList<Tarifa> leerTarifa(){
        ArrayList<Tarifa> tariInfo = new ArrayList<>();
        String csvFile = "tarifas.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
            String[] datos = line.split(cvsSplitBy);
            Tarifa tarifa1 = new Tarifa(datos[0], datos[1], datos[2],datos[3],datos[4]);
                tariInfo.add(tarifa1);
            }           
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }                       
        return tariInfo;
    }
    
    //PARA LLAMADAS
    public static ArrayList<Llamada> leerLlamada(){
        ArrayList<Llamada> llamadaInfo = new ArrayList<>();
        String csvFile = "llamadas.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
            String[] datos = line.split(cvsSplitBy);
            Llamada llamada1 = new Llamada(datos[0], datos[1], datos[2],datos[3],datos[4],datos[5],datos[6],datos[7]);
                llamadaInfo.add(llamada1);
            }           
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }                       
        return llamadaInfo;
    }
    
    //PARA PROVEEDORES
    public static ArrayList<Proveedor> leerProveedor(){
        ArrayList<Proveedor> proveeInfo = new ArrayList<>();
        String csvFile = "proveedores.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
            String[] datos = line.split(cvsSplitBy);
            Proveedor provee1 = new Proveedor(datos[0], datos[1], datos[2], datos[3]);
                proveeInfo.add(provee1);
            }           
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }                       
        return proveeInfo;
    }
    
    //ESCRITURA DE LLAMADAS_FACTURADAS.CSV
    public static void escribirLlamadas(String fecha, String hora, String ipF,
            String ipD, String dnis, String ani, String duracion, String codCliente,
            String codProvee, String codPais, float costoCliente, float costoProvee,String estado){
        final String NEXT_LINE = "\n"; //delimitador para salto de linea
        String delim =","; //separador de campos
        String csvFile = "archivos/llamadas_facturadas.csv";//nombre de archivo de salida
        try {
            FileWriter fw = new FileWriter(csvFile, true);//objeto filewriter
            //escribir en el archivo toda la informaci�n
            fw.append(fecha + delim + hora + delim + ipF + delim + ipD + delim+dnis
            + delim + ani + delim + duracion + delim + codCliente + delim + codProvee
            + delim + codPais.replaceAll("","") + delim + costoCliente +delim + costoProvee+delim + estado).append(System.lineSeparator());
            
            fw.flush();
            fw.close();//cerrar archivo  
        } catch (IOException e) {
            // Error al crear el archivo, por ejemplo, el archivo
            // est� actualmente abierto.
            e.printStackTrace();
	    }
    }
    
    //CONTAR NUMERO DE MESES FACTURADOS
    public static ArrayList<String> obtenerMeses(){
        ArrayList<Llamada> llamadaInfo = Lectura.leerLlamada();
        ArrayList<String> meses = new ArrayList<>();
        for(Llamada p: llamadaInfo){
            String[] fecha = p.getFecha().split("-");
            if (!meses.contains(fecha[1])){           
                meses.add(fecha[1]);
            }       
        }
        return meses;
    }
    
    //CREAR SUMARIO POR CLIENTE
    public static ArrayList<Cliente> crearSumarioC(){
        ArrayList<Llamada> llamadaInfo = Lectura.leerLlamada();
        ArrayList<Cliente> clienteInfo = Lectura.leerCliente();
        ArrayList<Ip> iPs = Lectura.leerIp();
        ArrayList<Cliente> clienteF = new ArrayList<>();
        String csvFile = "archivos/llamadas_facturadas.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        String mes="";String a�o="";String codCliente="";String codPais="";
        int llamadaInt=0; int llamadaComp=0; float totalPago=0;
        for (Cliente m: clienteInfo){          
            ArrayList<String> meses = Lectura.obtenerMeses();
            for(String n: meses) {
                totalPago =0; llamadaComp=0; llamadaInt =0;int contFinal=0;
                try {
                    br = new BufferedReader(new FileReader(csvFile));
                    while ((line = br.readLine()) != null) {
                        String[] datos = line.split(cvsSplitBy);
                        if (m.getCodigo().equals(datos[7])){
                            String[] fecha = datos[0].split("-");
                            //POR MES      
                            if (n.equals(fecha[1])){
                                mes = fecha[1]; a�o = fecha[0];
                                codCliente = datos[7]; codPais = datos[9];
                                totalPago += Float.parseFloat(datos[10]);                                                          
                            }                 
                        }
                
                    }                                      
                 } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if (br != null) {
                            try {
                                br.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }     
                for(Ip q: iPs){
                    if (q.getTipo().equals("C")& q.getCodigo().equals(m.getCodigo())){
                        for(Llamada p: llamadaInfo){
                            if (p.getIpF().equals(q.getIp())){
                                String[]fecha2 = p.getFecha().split("-");
                                if(fecha2[1].equals(n)){
                                    if (p.getEstado().equals("E")){
                                        llamadaInt++;
                                    } else if (p.getEstado().equals("N")){
                                        llamadaComp++;
                                    }
                                    contFinal++;
                                }  
                            }                                                                   
                        }
                    }                              
                }  
                if (contFinal!=0){
                    Cliente client1 = new Cliente(mes,a�o,codCliente,codPais,llamadaComp,llamadaInt,totalPago);
                    clienteF.add(client1);
                }               
            }           
        } 
        return clienteF;
    } 
    
    //CREAR SUMARIO POR PROVEEDOR
    public static ArrayList<Proveedor> crearSumarioP(){
        ArrayList<Llamada> llamadaInfo = Lectura.leerLlamada();
        ArrayList<Proveedor> proveeInfo = Lectura.leerProveedor();
        ArrayList<Ip> iPs = Lectura.leerIp();
        ArrayList<Proveedor> proveedorF = new ArrayList<>();
        String csvFile = "archivos/llamadas_facturadas.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        String mes="";String a�o="";String codProvee="";String codPais="";
        int llamadaInt=0; int llamadaComp=0; float totalPago=0;
        for (Proveedor m: proveeInfo){          
            ArrayList<String> meses = Lectura.obtenerMeses();
            for(String n: meses) {
                totalPago =0; llamadaComp=0; llamadaInt =0;int contFinal=0;
                try {
                    br = new BufferedReader(new FileReader(csvFile));
                    while ((line = br.readLine()) != null) {
                        String[] datos = line.split(cvsSplitBy);
                        if (m.getCodigo().equals(datos[8])){
                            String[] fecha = datos[0].split("-");
                            //POR MES      
                            if (n.equals(fecha[1])){
                                mes = fecha[1]; a�o = fecha[0];
                                codProvee = datos[8]; codPais = datos[9];
                                totalPago += Float.parseFloat(datos[11]);                                                          
                            }                 
                        }               
                    }                                      
                 } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if (br != null) {
                            try {
                                br.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }     
                for(Ip q: iPs){
                    if (q.getTipo().equals("P")& q.getCodigo().equals(m.getCodigo())){
                        for(Llamada p: llamadaInfo){
                            if (p.getIpD().equals(q.getIp())){
                                String[]fecha2 = p.getFecha().split("-");
                                if(fecha2[1].equals(n)){
                                    if (p.getEstado().equals("E")){
                                        llamadaInt++;
                                    } else if (p.getEstado().equals("N")){
                                        llamadaComp++;
                                    }
                                    contFinal++;
                                }  
                            }                                                                   
                        }
                    }                              
                }  
                if (contFinal!=0){
                    Proveedor provee1 = new Proveedor(mes,a�o,codProvee,codPais,llamadaComp,llamadaInt,totalPago);
                    proveedorF.add(provee1);
                }               
            }           
        } 
        return proveedorF;
    } 
}