package Metodos;
import Inicializacion.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Metodo {
    Scanner entrada = new Scanner(System.in);   
    
    //VERIFICAR EL INICIO DE SESIÓN
    public boolean verificarLogin(ArrayList<Usuario> userInfo, String usuario, String contraseña){
        boolean verificacion; int valor=0;
        for(Usuario p: userInfo){
            valor = 0;
            if (usuario.equals(p.getUsuario())){
                valor++;
            } if(contraseña.equals(p.getContraseña())){
                valor++;  
            } if(valor ==2){
                break;
            }
        }
        
        if(valor ==2){
            verificacion = true;
        }
        else {
            verificacion = false;
        }
        return verificacion;
    }
    
    //VERIFICAR SI UN DATO ES NUMÉRICO
    public boolean isNumeric(String cadena){
        try {
		Integer.parseInt(cadena);
		return true;
	} catch (NumberFormatException e){
		return false;
	}
    }
    
    //VERIFICAR SI UN DATO ES UN NUMERO TELÉFONICO
    public boolean isTelef(String cadena){
        Metodo metodos = new Metodo();
        if ((metodos.isNumeric(cadena)) & (cadena.length())>=9){
            return true;
        } else{
            return false;
        }
    }
    
    //VERIFICAR SI UN DATO ES UNA TARIFA
    public boolean isTarifa(String cadena){
        try{
            Float.parseFloat(cadena);
            return true;
        }catch(NumberFormatException e){
            return false;
        }
    }
           
    //OBTENER OBJETO DEL USUARIO QUE HA HECHO LOGIN
    public Usuario obtenerUser(ArrayList<Usuario> userInfo, String usuario){
        Usuario info = new Usuario("","","","");
        for(Usuario p: userInfo){
            if (usuario.equals(p.getUsuario())){
                info = p;
            }
        }
        return info;     
    }
    
    //OBTENER OBJETO DE UN CLIENTE
    public Cliente obtenerCliente(ArrayList<Cliente> clienteInfo, String cod){
        Cliente info = new Cliente("","","","","");
        for(Cliente p: clienteInfo){
            if (cod.equals(p.getCodigo())){
                info = p;
            }
        }
        return info;     
    }
    
    //OBTENER OBJETO DE UN PROVEEDOR
    public Proveedor obtenerProveedor( ArrayList<Proveedor> proveedorInfo, String cod){
        Proveedor info = new Proveedor("","","","");
        for(Proveedor p: proveedorInfo){
            if (cod.equals(p.getCodigo())){
                info = p;
            }
        }
        return info;     
    }
    
    //OBTENER OBJETO DE UN PAIS
    public Pais obtenerPais( ArrayList<Pais> paisInfo, String cod){
        Pais info = new Pais("","","");
        for(Pais p: paisInfo){
            if (cod.equals(p.getCodigo())){
                info = p;
            }
        }
        return info;     
    }
    
    //OBTENER OBJETO DE UNA IP
    public Ip obtenerIp( ArrayList<Ip> ipInfo, String cod, String asignacion){
        Ip info = new Ip("","","");
        for(Ip p: ipInfo){
            if (cod.equals(p.getCodigo()) & asignacion.equals(p.getTipo())){
                info = p;
            }
        }
        return info;     
    }
    
    //OBTENER OBJETO DE UNA TARIFA
    public Tarifa obtenerTarifa(ArrayList<Tarifa> tarifaInfo, String cod){
        Tarifa info = new Tarifa("","","","","","");
        for(Tarifa p: tarifaInfo){
            if (cod.equals(p.getCodTarifa())){
                info = p;
            }
        }
        return info;     
    }
    
    //GENERAR STR DEL MENÚ DE TECNICO
    public String obtenerMenuT(){
        String strMenu = "\t1. Información de Clientes\n" +
                         "\t2. Información de Proveedores\n" +
                         "\t3. Información de Países\n" +
                         "\t4. Administrar IPs\n" +
                         "\t5. Información de Tarifa de Región\n" +
                         "\t6. Facturar llamadas\n"+
                         "\t7. Salir";
        return strMenu;
    }
    
    //GENERAR STR DEL MENÚ DE ADMIN
    public String obtenerMenuA(){
        String strMenu = "\t1. Detalle Factura Cliente\n" +
                         "\t2. Reporte llamadas por Clientes por Mes\n" +
                         "\t3. Reporte llamadas por Proveedor por Mes\n"+
                         "\t4. Salir";
        return strMenu;
    }
    
    //IMPRIMIR DATOS DE UN CLIENTE
    public void imprimirCliente(ArrayList<Cliente> clienteInfo, String cod){
        Metodo metodos = new Metodo();
        Cliente cliente1 = metodos.obtenerCliente(clienteInfo, cod);
        System.out.println("\n\n\tNombre: "+cliente1.getNombre()+
            "\n\tDireccion: "+cliente1.getDireccion()+
            "\n\tTelefono: "+cliente1.getTelefono()+
            "\n\tTipo: "+cliente1.getTipo());
    }
    
    //IMPRIMIR DATOS DE TODOS LOS CLIENTES
    public void imprimirClientes(ArrayList<Cliente> clienteInfo){
        for (Cliente p: clienteInfo){
            System.out.println("\n\n\tCodigo: "+p.getCodigo()+p.getCodigo()+"\tNombre: "+p.getNombre()+
            "\tDireccion: "+p.getDireccion()+ "\tTelefono: "+p.getTelefono()+
            "\tTipo: "+p.getTipo()+"\n--------------------------");
        }                
    }
    
    //IMPRIMIR DATOS DE TODOS LOS PROVEEDORES
    public void imprimirProveedores(ArrayList<Proveedor> proveedorInfo){
        for (Proveedor p: proveedorInfo){
            System.out.println("\n\n\tCodigo: "+p.getCodigo()+"\tNombre: "+p.getNombre()+
            "\tDireccion: "+p.getDireccion()+ "\tTelefono: "+p.getTelefono()+
            "\n--------------------------");
        }                
    }
    
    //IMPRIMIR DATOS DE TODOS LOS PAISES
    public void imprimirPaises(ArrayList<Pais> paisInfo){
        for (Pais p: paisInfo){
            System.out.println("\n\n\tCodigo: "+p.getCodigo()+"\tNombre: "+p.getNombre()+
            "\tPrefijo: "+p.getPrefijo()+"\n--------------------------");
        }                
    }
    
    //IMPRIMIR DATOS DE TODOS LOS IPs
    public void imprimirIps(ArrayList<Ip> ipInfo){
        for (Ip p: ipInfo){
            System.out.println("\n\n\tIp: "+p.getIp()+"\tTipo: "+p.getTipo()+
            "\tCodigo: "+p.getCodigo()+"\n--------------------------");
        }                
    }
    
    //IMPRIMIR DATOS DE TODAS LOS TARIFAS
    public void imprimirTarifas(ArrayList<Tarifa> tarifaInfo,ArrayList<Proveedor> proveedorInfo){
        for (Tarifa p: tarifaInfo){
            String nombreProvee="";
            for (Proveedor m: proveedorInfo){
                if (p.getcodigoPro().equals(m.getCodigo())){
                    System.out.println("\n\n\tCódigo de Tarifa: "+p.getCodTarifa()+"\tCódigo País: "+p.getprefijoPais()+
                    "\tNombre Región: "+p.getnombreRegion()+ "\tPrefijo Región: "+p.getPrefijoRegion()+
                    "\tTarifa: "+p.getTarifa()+"\tNombre Proveedor: "+nombreProvee+"\n--------------------------");
                }
            }      
        }                
    }
    
    //VERIFICAR QUE UN CODIGO INGRESADO DE CLIENTE ESTÉ EN EL CSV
    public boolean verificarCliente(ArrayList<Cliente> clienteInfo, String codigo){
        boolean verificacion= false;
        for(Cliente p: clienteInfo){
            if (codigo.equals(p.getCodigo())){
                verificacion = true;
                break;
            }
        }
        return verificacion;
    }
    
    //VERIFICAR QUE UN CODIGO INGRESADO DE PROVEEDOR ESTÉ EN EL CSV
    public boolean verificarProveedor(ArrayList<Proveedor> proveedorInfo, String codigo){
        boolean verificacion= false;
        for(Proveedor p: proveedorInfo){
            if (codigo.equals(p.getCodigo())){
                verificacion = true;
                break;
            }
        }
        return verificacion;
    }
    
    //VERIFICAR QUE UN CODIGO INGRESADO DE PAIS ESTÉ EN EL CSV
    public boolean verificarPais(ArrayList<Pais> paisInfo, String codigo){
        boolean verificacion= false;
        for(Pais p: paisInfo){
            if (codigo.equals(p.getCodigo())){
                verificacion = true;
                break;
            }
        }
        return verificacion;
    }
    
    //VERIFICAR QUE UNA IP SEA VALIDA, NO ESTÉ ASOCIADA Y QUE EL CODIGO DE PERSONA ESTÉ CORRECTO
    public boolean verificarIp(ArrayList<Ip> ipInfo, String ip,ArrayList<Cliente> clienteInfo,
            ArrayList<Proveedor> proveedorInfo, String asignacion, String codigo){
        Metodo metodos = new Metodo();
        int contador = 0; boolean verificacion1 = false; boolean verificacion2 = false;
        
        //verificar que no sea una ip existente
        for(Ip p: ipInfo){
            if (ip.equals(p.getIp())){
                contador++;
            }
        //verificar que el codigo ingresado sea de un cliente existente
        }if (asignacion.equalsIgnoreCase("C")){
            verificacion1 = metodos.verificarCliente(clienteInfo, codigo);
            
        //verificar que el codigo ingresado sea de un proveedor existente
        }else if (asignacion.equalsIgnoreCase("P")){           
            verificacion1 = metodos.verificarProveedor(proveedorInfo, codigo);
        }
        
        //verificar que la ip tenga el formato adecuado
        
        try{           
            String parts[]= ip.split(".");
            if(parts.length==4){               
                verificacion2 = true;
            }else{
                verificacion2 = false;
            }
                       
        }catch(NumberFormatException e){
            verificacion2 = false;
        }
        if (verificacion2){
            String parts[]= ip.split(".");
            verificacion2= metodos.isNumeric(parts[0]) & metodos.isNumeric(parts[1])
                    & metodos.isNumeric(parts[2])& metodos.isNumeric(parts[3]);
        }
        
               
        return (verificacion1 & contador==0 & verificacion2);
    }
    
    //VERIFICAR QUE UNA IP EXISTA
    public boolean verificarIpEx(ArrayList<Ip> ipInfo, String asignacion, String codigo){
        boolean verificacion= false;
        for(Ip p: ipInfo){
            if (codigo.equals(p.getCodigo()) & asignacion.equals(p.getTipo())){
                verificacion = true;
                break;
            }
        }
        return verificacion;
    }
    
    
    //VERIFICAR QUE UN CODIGO INGRESADO DE TARIFA ESTÉ EN EL CSV
    public boolean verificarTarifa(ArrayList<Tarifa> tarifaInfo, String codigo){
        boolean verificacion= false;
        for(Tarifa p: tarifaInfo){
            if (codigo.equals(p.getCodTarifa())){
                verificacion = true;
                break;
            }
        }
        return verificacion;
    }
    
    //GENERAR STR DEL MENÚ DE EDICION DE CLIENTES
    public void menuClientes(){
        System.out.println("\n¿Qué sección desea editar?"
                + "\n\t1. Código\n\t2. Nombre\n\t3. Dirección"
                + "\n\t4. Teléfono\n\t5. Tipo de Cliente"
                + "\n\t6. Salir");
    }
    
    //MENÚ DE EDICION DE CLIENTES DESARROLLADO (MENU TECNICO OPCION 1)
    public void elegirdeClientes(ArrayList<Cliente> clienteInfo){
        Metodo metodos = new Metodo();
        metodos.imprimirClientes(clienteInfo);
        int cont1 =0;int cont2 = 0;int cont3 = 0;
        while (cont1!=1){            
            System.out.println("\n¿Desea editar la Información de los Clientes? (Y/N)\n");
            String decision1 = entrada.next();
            if(decision1.equalsIgnoreCase("Y")){
                metodos.imprimirClientes(clienteInfo); cont2 = 0;
                while(cont2!=1){
                    System.out.println("\nIngrese el código del cliente que desea editar: \n");
                    String cod = entrada.next();
                    if(metodos.verificarCliente(clienteInfo, cod)){ 
                        Cliente cliente1 = metodos.obtenerCliente(clienteInfo, cod);
                        metodos.imprimirCliente(clienteInfo, cod);                                                
                        metodos.menuClientes();
                        String decision2 = entrada.next();
                        Scanner entrada2 = new Scanner(System.in);
                        switch (decision2){
                            case "1":  
                                System.out.println("\nEscriba un nuevo código para su Cliente: \n");
                                String newCod= entrada.next();                
                                if (metodos.isNumeric(newCod)){
                                    cliente1.setCodigo(newCod);
                                    metodos.imprimirClientes(clienteInfo);
                                }else{
                                    System.out.println("Código No Válido");
                                }                                
                                cont2 = 1;
                                break;
                                
                            case "2":
                                System.out.println("\nEscriba un nuevo Nombre para su Cliente: ");
                                String newName= entrada2.nextLine(); 
                                cliente1.setNombre(newName);
                                metodos.imprimirClientes(clienteInfo);
                                cont2 = 1;
                                break;
                                
                            case "3":
                                System.out.println("\nEscriba una nueva Dirección para su Cliente: \n");
                                String newDirecc= entrada2.nextLine(); 
                                cliente1.setDireccion(newDirecc);
                                metodos.imprimirClientes(clienteInfo);
                                cont2 = 1;
                                break;
                                
                            case "4":
                                System.out.println("\nEscriba un nuevo Número de Teléfono para su Cliente: \n");
                                String newTel= entrada.next(); 
                                //verificar que sea numero mayor o igual a 10
                                if (metodos.isTelef(newTel)){
                                    cliente1.setTelefono(newTel);
                                    metodos.imprimirClientes(clienteInfo);
                                }else{
                                    System.out.println("Número No Válido");
                                }        
                                cont2 = 1;                               
                                break;
                            
                            case "5":
                                System.out.println("\nEscriba el nuevo Tipo de Cliente \n");
                                String newTipo= entrada.next();                
                                if (newTipo.equalsIgnoreCase("Wholesale")){
                                    cliente1.setTipo(newTipo);
                                    metodos.imprimirClientes(clienteInfo);
                                }else if(newTipo.equalsIgnoreCase("Retail")) {
                                    cliente1.setTipo(newTipo);
                                    metodos.imprimirClientes(clienteInfo);
                                }else{
                                    System.out.println("Ingreso no Válido");
                                } 
                                cont2 = 1;
                                break;
                                
                            case "6":
                                cont2 = 1;
                                break;
                                
                            default:
                                System.out.println("\nSelección Incorrecta");
                                cont2 = 1;
                                break;                           
                        }
                    }else{
                        System.out.println("Código No Válido");
                    }
                }
            }else if(decision1.equalsIgnoreCase("N")){
                cont1++;
            }else{
                System.out.println("\nEntrada Incorrecta\n");
            }         
        }         
    }
    
    //GENERAR STR DEL MENÚ DE EDICION DE PROVEEDORES
    public void menuProveedores(){
        System.out.println("\n¿Qué sección desea editar?"
                + "\n\t1. Código\n\t2. Nombre\n\t3. Dirección"
                + "\n\t4. Teléfono\n\t5. Salir");
    }
    
    //MENÚ DE EDICION DE PROVEEDORES DESARROLLADO (MENU TECNICO OPCION 2)
    public void elegirdeProveedores(ArrayList<Proveedor> proveedorInfo){
        Metodo metodos = new Metodo();
        int cont1 =0;int cont2 = 0;int cont3 = 0;
        while (cont1!=1){            
            System.out.println("\n¿Desea editar la Información de los Proveedores? (Y/N)\n");
            String decision1 = entrada.next();
            if(decision1.equalsIgnoreCase("Y")){
                metodos.imprimirProveedores(proveedorInfo); cont2 = 0;
                while(cont2!=1){
                    System.out.println("\nIngrese el código del Proveedor que desea editar: \n");
                    String cod = entrada.next();
                    if(metodos.verificarProveedor(proveedorInfo, cod)){ 
                        Proveedor proveedor1 = metodos.obtenerProveedor(proveedorInfo, cod);
                        metodos.imprimirProveedores(proveedorInfo);                                                
                        metodos.menuProveedores();
                        String decision2 = entrada.next();
                        Scanner entrada2 = new Scanner(System.in);
                        switch (decision2){
                            case "1":  
                                System.out.println("\nEscriba un nuevo código para su Cliente: \n");
                                String newCod= entrada.next();                
                                if (metodos.isNumeric(newCod) & !metodos.verificarProveedor(proveedorInfo, newCod)){
                                    proveedor1.setCodigo(newCod);
                                    metodos.imprimirProveedores(proveedorInfo);
                                }else{
                                    System.out.println("Código No Válido");
                                }                                
                                cont2 = 1;
                                break;
                                
                            case "2":
                                System.out.println("\nEscriba un nuevo Nombre para su Proveedor: ");
                                String newName= entrada2.nextLine(); 
                                proveedor1.setNombre(newName);
                                metodos.imprimirProveedores(proveedorInfo);
                                cont2 = 1;
                                break;
                                
                            case "3":
                                System.out.println("\nEscriba una nueva Dirección para su Proveedor: \n");
                                String newDirecc= entrada2.nextLine(); 
                                proveedor1.setDireccion(newDirecc);
                                metodos.imprimirProveedores(proveedorInfo);
                                cont2 = 1;
                                break;
                                
                            case "4":
                                System.out.println("\nEscriba un nuevo Número de Teléfono para su Proveedor: \n");
                                String newTel= entrada.next(); 
                                //verificar que sea numero mayor o igual a 10
                                if (metodos.isTelef(newTel)){
                                    proveedor1.setTelefono(newTel);
                                    metodos.imprimirProveedores(proveedorInfo);
                                }else{
                                    System.out.println("Número No Válido");
                                }        
                                cont2 = 1;                               
                                break;
                                                         
                            case "5":
                                cont2 = 1;
                                break;
                                
                            default:
                                System.out.println("\nSelección Incorrecta");
                                cont2 = 1;
                                break;                           
                        }
                    }else{
                        System.out.println("Código No Válido");
                    }
                }
            }else if(decision1.equalsIgnoreCase("N")){
                cont1++;
            }else{
                System.out.println("\nEntrada Incorrecta\n");
            }         
        }         
    }
    
    //GENERAR STR DEL MENÚ DE EDICION DE PAISES
    public void menuPaises(){
        System.out.println("\n¿Qué sección desea editar?"
                + "\n\t1. Código\n\t2. Nombre\n\t3. Prefijo\n\t4. Salir");
    }
    
    //MENÚ DE EDICION DE PAISES DESARROLLADO (MENU TECNICO OPCION 3)
    public void elegirdePaises( ArrayList<Pais> paisInfo){
        Metodo metodos = new Metodo();
        int cont1 =0;int cont2 = 0;int cont3 = 0;
        while (cont1!=1){            
            System.out.println("\n¿Desea editar la Información de los Paises? (Y/N)\n");
            String decision1 = entrada.next();
            if(decision1.equalsIgnoreCase("Y")){
                metodos.imprimirPaises(paisInfo); cont2 = 0;
                while(cont2!=1){
                    System.out.println("\nIngrese el código del Pais que desea editar: \n");
                    String cod = entrada.next();
                    if(metodos.verificarPais(paisInfo, cod)){ 
                        Pais pais1 = metodos.obtenerPais(paisInfo, cod);
                        metodos.imprimirPaises(paisInfo);                                                
                        metodos.menuPaises();
                        String decision2 = entrada.next();
                        Scanner entrada2 = new Scanner(System.in);
                        switch (decision2){
                            case "1":  
                                System.out.println("\nEscriba un nuevo código para este Pais: \n");
                                String newCod= entrada.next();                
                                if (metodos.isNumeric(newCod) & !metodos.verificarPais(paisInfo, newCod)){
                                    pais1.setCodigo(newCod);
                                    metodos.imprimirPaises(paisInfo);
                                }else{
                                    System.out.println("Código No Válido");
                                }                                
                                cont2 = 1;
                                break;
                                
                            case "2":
                                System.out.println("\nEscriba un nuevo Nombre para este Pais: ");
                                String newName= entrada2.nextLine(); 
                                pais1.setNombre(newName);
                                metodos.imprimirPaises(paisInfo);
                                cont2 = 1;
                                break;
                                
                            case "3":
                                System.out.println("\nEscriba un nuevo Prefijo para este Pais: \n");
                                String newPrefijo= entrada2.nextLine(); 
                                pais1.setPrefijo(newPrefijo);
                                metodos.imprimirPaises(paisInfo);
                                cont2 = 1;
                                break;
                                
                            case "4":
                                cont2 = 1;
                                break;
                                                                                      
                            default:
                                System.out.println("\nSelección Incorrecta");
                                cont2 = 1;
                                break;                           
                        }
                    }else{
                        System.out.println("Código No Válido");
                    }
                }
            }else if(decision1.equalsIgnoreCase("N")){
                cont1++;
            }else{
                System.out.println("\nEntrada Incorrecta\n");
            }         
        }         
    }
    
    //GENERAR STR DEL MENÚ DE EDICION DE IPs 
    public void menuIps(){
        System.out.println("\n¿Qué desea hacer?"
                + "\n\t1. Agregar IP\n\t2. Editar IP\n\t3. Salir");
    }
       
    //MENÚ DE EDICION DE IPS DESARROLLADO (MENU TECNICO OPCION 4)
    public void elegirdeIps( ArrayList<Ip> ipInfo,ArrayList<Cliente> clienteInfo,ArrayList<Proveedor> proveedorInfo){
        Metodo metodos = new Metodo();
        int cont1 =0;int cont2 = 0;int cont3 = 0; String newIp,newAsig,newCod,asignacion,codigo;
        while (cont1!=1){ 
            if (cont2!=1){
                metodos.imprimirIps(ipInfo);
            }
            
            metodos.menuIps();
            String decision1 = entrada.next();
            switch (decision1){
                //
                case "1":  
                    System.out.println("\nEscriba una nueva IP para agregar: \n");
                    newIp= entrada.next();  
                    System.out.println("\nEscriba el tipo de asignación (C/P): \n");
                    newAsig= entrada.next(); 
                    System.out.println("\nEscriba el codigo para su Cliente/Proveedor: \n");
                    newCod= entrada.next(); 
                    if (metodos.verificarIp(ipInfo, newIp, clienteInfo, proveedorInfo, newAsig, newCod)){
                        Ip ip1 = new Ip("newIp","newAsig","newCod");
                        ipInfo.add(ip1);  
                        cont2=0;
                    }else{
                        System.out.println("Datos Incorrectos");
                        cont2=1;
                    }                   
                    break;
                                                       
                //
                case "2":
                    System.out.println("\nEscriba el tipo de asignación (C/P) que desea editar: \n");
                    asignacion= entrada.next(); 
                    System.out.println("\nEscriba el codigo del Cliente/Proveedor existente: \n");
                    codigo= entrada.next(); 
                    if (metodos.verificarIpEx(ipInfo, asignacion, codigo)){                       
                        System.out.println("\nEscriba el nuevo tipo de asignación (C/P): \n");
                        newAsig= entrada.next(); 
                        System.out.println("\nEscriba el nuevo codigo del Cliente/Proveedor: \n");
                        newCod= entrada.next();
                        if (!metodos.verificarIpEx(ipInfo, newAsig, newCod)){
                            Ip ip1 = metodos.obtenerIp(ipInfo, codigo, asignacion);
                            ip1.setTipo(newAsig); ip1.setCodigo(newCod); 
                            cont2=0;
                        }else{
                            System.out.println("Datos Incorrectos");
                            cont2=1;
                        }

                    }else{
                        System.out.println("Datos Incorrectos");
                        cont2=1;
                    }                 
                    break;
                                
                case "3":
                    cont1 = 1;
                    break;                              
                                                                                      
                default:
                    System.out.println("\nSelección Incorrecta");
                    cont2=1;
                    break;                                  
            }         
        }
    }    
    
    //MENÚ DE EDICION DE TARIFAS DESARROLLADO (MENU TECNICO OPCION 5)
    public void elegirdeTarifas( ArrayList<Tarifa> tarifaInfo,ArrayList<Proveedor> proveedorInfo){
        Metodo metodos = new Metodo();
        int cont1 =0;int cont2 = 0;int cont3 = 0;
        while (cont1!=1){            
            System.out.println("\n¿Desea editar alguna Tarifa? (Y/N)\n");
            String decision1 = entrada.next();
            if(decision1.equalsIgnoreCase("Y")){
                metodos.imprimirTarifas(tarifaInfo,proveedorInfo); cont2 = 0;
                while(cont2!=1){
                    System.out.println("\nIngrese el código de la tarifa que desea editar: \n");
                    String cod = entrada.next();
                    if(metodos.verificarTarifa(tarifaInfo, cod)){ 
                        Tarifa tarifa1 = metodos.obtenerTarifa(tarifaInfo, cod);
                        System.out.println("\nIngrese la tarifa nueva: \n");
                        String newTarifa = entrada.next();
                        if (metodos.isTarifa(newTarifa)){
                            tarifa1.setTarifa(newTarifa);
                            metodos.imprimirTarifas(tarifaInfo,proveedorInfo);
                        }else{
                            System.out.println("Valor Incorrecto");
                            cont2++;
                        }                                          
                    }else{
                        System.out.println("Código No Válido");
                    }
                }
            }else if(decision1.equalsIgnoreCase("N")){
                cont1++;
            }else{
                System.out.println("\nEntrada Incorrecta\n");
            }         
        }         
    }
    
    //FACTURAR LLAMADAS (MENU TECNICO OPCION 6)
    public void facturarLlamadas(ArrayList<Ip> ipInfo, ArrayList<Tarifa> tarifaInfo,ArrayList<Cliente> clienteInfo){
        ArrayList<Llamada> llamadaInfo = Lectura.leerLlamada();
        ArrayList<Pais> paisInfo = Lectura.leerPais();
        for(Llamada m: llamadaInfo){
            String codCliente="",codProvee="",codPais="", tipoC=""; 
            float costoCliente=0; float costoProvee=0; 
            String fecha = m.getFecha(); String hora = m.getHora();
            String ipF = m.getIpF(); String ipD = m.getIpD();
            String dnis = m.getDNIS(); String ani = m.getANI();
            String estado = m.getEstado(); String duracion = m.getDuracion();
            String estadoNum;
            if (estado.equals("E")) {
                costoProvee = 0;costoCliente =0;   
                estadoNum = "0";
            }else {
                estadoNum = "1";
            }
            for (Ip n: ipInfo){
                if (ipF.equals(n.getIp())) codCliente = n.getCodigo();  
                else if (ipD.equals(n.getIp())) codProvee = n.getCodigo();              
            }
            for (Tarifa p: tarifaInfo){
                if (dnis.startsWith(p.getPrefijoRegion())){
                    String prefPais = p.getprefijoPais(); 
                    for (Pais q: paisInfo){
                        if (prefPais.equals(q.getPrefijo())) codPais = q.getCodigo();
                    }
                    if (!estado.equals("E")){
                        costoProvee = (Float.parseFloat(duracion)/60)*Float.parseFloat(p.getTarifa());
                    }  
                    break;
                }
                else{
                    codPais = "NA"; costoProvee = 0; costoCliente =0;
                }
            } 
            for(Cliente q: clienteInfo){
                if (codCliente.equals(q.getCodigo())) tipoC = q.getTipo();                       
                if (tipoC.equalsIgnoreCase("Wholesale") & !estado.equals("E")){
                    costoCliente = (costoProvee+=((float)(costoProvee*5)/100));
                }
                else if(tipoC.equalsIgnoreCase("Retail")& !estado.equals("E")){
                    costoCliente =(costoProvee+=((float)(costoProvee*10)/100));
                }
            }  
            Lectura.escribirLlamadas(fecha, hora, ipF, ipD, dnis, ani, duracion, codCliente, codProvee, codPais, costoCliente, costoProvee, estadoNum);
        } 
    }

    //GENERAR DETALLE DE FACTURA (ADMIN OPCION 1)
    public void mostrarFactura(ArrayList<Cliente> clienteInfo, ArrayList<Cliente> listClienteF, ArrayList<String> listMeses){
        Metodo metodos = new Metodo();        
        System.out.println("Ingrese el codigo del cliente que desea generar Factura: ");
        String codIngreso = entrada.next(); 
        while(!metodos.verificarCliente(clienteInfo, codIngreso)){
            System.out.println("\nCódigo Incorrecto");
            System.out.println("\nIngrese el codigo del cliente que desea generar Factura: ");
            codIngreso = entrada.next();
        }
        System.out.println("\nIngrese el mes que desea facturar (formato 2 digitos): ");                                 
        String mesStr = entrada.next();
        if (!listMeses.contains(mesStr)){
            System.out.println("\tError, Mes no Encontrado");
        }else{                                       
            for (Cliente m: listClienteF){
                String nombreCliente="", tipo=""; float totalLlamFact=0, totalCosto=0, totalPago=0; int cont3 =0,llamInte,llamComp;
                if (m.getCodigo().equals(codIngreso) & m.getMes().equals(mesStr)){
                    for(Cliente n: clienteInfo){
                        if(m.getCodigo().equals(n.getCodigo())){
                            nombreCliente= n.getNombre();
                            tipo = n.getTipo();
                            cont3++;
                        }
                    }
                }
                llamInte = m.getLlamInte(); llamComp = m.getLlamComp();
                totalLlamFact = m.getTotalPago();
                if (tipo.equals("Wholesale")){
                    totalCosto=20;                                            
                }else{
                    totalCosto=0;
                }
                totalPago=totalLlamFact+totalCosto;
                                            
                if (cont3!=0){
                    System.out.println("--------------------------");
                    System.out.println("\tCliente: "+nombreCliente);
                    System.out.println("\tLlamadas completadas:  "+llamComp);
                    System.out.println("\tIntentos de llamadas:  "+llamInte);                   
                    System.out.println("\tTotal llamadas facturadas: $ "+totalLlamFact);
                    System.out.println("\tTotal costo de servicio: $ "+totalCosto);
                    System.out.println("\tTotal a pagar en el mes: $ "+totalPago);
                    System.out.println("--------------------------");
                }                                            
            }                                      
        }
    }
    
    //REPORTE DE CUENTAS PRO MES (MENU ADMIN OPCION 2)
    public void mostrarReporteMesC(ArrayList<Cliente> clienteInfo, ArrayList<Cliente> listClienteF, ArrayList<String> listMeses,ArrayList<Pais> paisInfo){
        System.out.println("\nIngrese el mes que desea consultar (formato 2 digitos): ");                                 
        String mesStr = entrada.next();
        while (!listMeses.contains(mesStr)){
            System.out.println("\tError, Mes no Encontrado");
            System.out.println("\nIngrese el mes que desea consultar (formato 2 digitos): ");                                 
            mesStr = entrada.next();
        }
        for (Cliente m: listClienteF){
            String nombreCliente="", pais="", tipo=""; float totalLlamFact=0, 
            totalCosto=0, totalPago=0; int cont3 =0, llamInte =0, llamComp = 0;
            if (m.getMes().equals(mesStr)){
                for(Cliente n: clienteInfo){
                    if(m.getCodigo().equals(n.getCodigo())){                          
                        nombreCliente= n.getNombre();
                        tipo = n.getTipo();
                        cont3++;
                    }
                }
            }
            llamInte = m.getLlamInte(); llamComp = m.getLlamComp();
            for (Pais n: paisInfo){
                if (m.getCodPais().equals(n.getCodigo())){
                    pais = n.getNombre();
                }
            }
            totalLlamFact = m.getTotalPago();
            if (tipo.equals("Wholesale")){
                totalCosto=20;                                            
            }else{
                totalCosto=0;
            }
            totalPago=totalLlamFact+totalCosto;                               
            if (cont3!=0){
                System.out.println("------------------------------");
                System.out.println("\tNombre de Cliente: "+nombreCliente);
                System.out.println("\tPaís:  "+pais);
                System.out.println("\tLlamadas completadas:  "+llamComp);
                System.out.println("\tIntentos de llamadas:  "+llamInte);
                System.out.println("\tTotal a pagar en el mes: $ "+totalPago);
                System.out.println("------------------------------");
            }                                            
        }     
    }
    
    public void mostrarReporteMesP(ArrayList<Proveedor> proveedorInfo, ArrayList<Proveedor> listProveeF, ArrayList<String> listMeses,ArrayList<Pais> paisInfo){
        System.out.println("\nIngrese el mes que desea consultar (formato 2 digitos): ");                                 
        String mesStr = entrada.next();
        while (!listMeses.contains(mesStr)){
            System.out.println("\tError, Mes no Encontrado");
            System.out.println("\nIngrese el mes que desea consultar (formato 2 digitos): ");                                 
            mesStr = entrada.next();
        }
        for (Proveedor m: listProveeF){
            String nombreProvee="", pais="", tipo=""; float totalPago=0; 
            int cont3 =0, llamInte =0, llamComp = 0;
            if (m.getMes().equals(mesStr)){
                for(Proveedor n: proveedorInfo){
                    if(m.getCodigo().equals(n.getCodigo())){                          
                        nombreProvee= n.getNombre();
                        cont3++;
                    }
                }
            }
            llamInte = m.getLlamInte(); llamComp = m.getLlamComp();
            for (Pais n: paisInfo){
                if (m.getCodPais().equals(n.getCodigo())){
                    pais = n.getNombre();
                }
            }
            
            totalPago = m.getTotalPago();                                        
            if (cont3!=0){
                System.out.println("------------------------------");
                System.out.println("\tNombre de Proveedor: "+nombreProvee);
                System.out.println("\tPaís:  "+pais);
                System.out.println("\tLlamadas completadas:  "+llamComp);
                System.out.println("\tIntentos de llamadas:  "+llamInte);
                System.out.println("\tTotal a pagar en el mes: $ "+totalPago);
                System.out.println("------------------------------");
            }                                            
        }     
    }
}
